﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neo4j.Driver;
using NPB_Neo4J.DataLayer.Models;
using Neo4jClient.Cypher;
using Neo4jClient;

namespace NPB_Neo4J.DataLayer
{
    public class DataProvider
    {
        private readonly GraphClient client;

        public DataProvider()
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "", "");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public List<Kurs> VratiSveKurseve()
        {
            var query = client.Cypher.Match("(k:Kurs)").Return(k => new { Kurs = k.As<Kurs>() });

            var lista = new List<Kurs>();

            foreach (var result in query.Results)
                lista.Add(result.Kurs);

            return lista;
        }

        public List<Profesor> VratiSveProfesore()
        {
            var query = client.Cypher.Match("(k:Profesor)").Return(k => new { Profesor = k.As<Profesor>() });

            var lista = new List<Profesor>();

            foreach (var result in query.Results)
                lista.Add(result.Profesor);

            return lista;
        }

        public List<Polaznik> VratiSvePolaznike()
        {
            var query = client.Cypher.Match("(k:Polaznik)").Return(k => new { Polaznik = k.As<Polaznik>() });

            var lista = new List<Polaznik>();

            foreach (var result in query.Results)
                lista.Add(result.Polaznik);

            return lista;
        }

        public void DodajKurs(Kurs kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);
            dictionary.Add("BrojPolaznika", kurs.BrojPolaznika);

            var proveri_postojeci = client.Cypher.Match("(k:Kurs)").WithParams(dictionary).Where("k.Jezik={Jezik} AND k.Nivo = {Nivo}").Return(k => new { Kurs = k.As<Kurs>() });

            if (proveri_postojeci.Results.Any())
                throw new Exception("Kurs " + kurs.Jezik + " " + kurs.Nivo + " vec postoji.");

            client.Cypher.Create("(:Kurs{Jezik : {Jezik}, Nivo: {Nivo}, BrojPolaznika: {BrojPolaznika}})")
                  .WithParams(dictionary).ExecuteWithoutResults();
        }

        public List<Kurs> PretraziKurs(Kurs kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            var query = client.Cypher.Match("(k:Kurs)")
                .WithParams(dictionary)
                .Where("(({Jezik} IS NULL) OR k.Jezik={Jezik}) AND (({Nivo} IS NULL) OR k.Nivo={Nivo})")
                .Return(k => new { Kurs = k.As<Kurs>() });

            var lista = new List<Kurs>();
            foreach (var result in query.Results)
                lista.Add(result.Kurs);

            return lista;
        }

        public Profesor VratiProfesora(Kurs kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            var query = client.Cypher.Match("(k:Kurs),(p:Profesor)")
                .WithParams(dictionary)
                .Where("k.Jezik = {Jezik} AND k.Nivo = {Nivo} ")
                .Match("(p)-[:Drzi]->(k)")
                .Return(p => new { Profesor = p.As<Profesor>() });

            if (query.Results.Any())
                return query.Results.FirstOrDefault().Profesor;

            return null;
        }

        public Profesor VratiProfesora(Profesor profesor)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Email", profesor.Email);

            var query = client.Cypher.Match("(p:Profesor {Email: {Email}})")
                .WithParams(dictionary)
                .Return(p => new { Profesor = p.As<Profesor>() });

            if (query.Results.Any())
                return query.Results.FirstOrDefault().Profesor;

            return null;
        }

        public List<Polaznik> VratiPolaznike(Kurs kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            var query = client.Cypher.Match("(k:Kurs),(p:Polaznik)")
                .WithParams(dictionary)
                .Where("k.Jezik = {Jezik} AND k.Nivo = {Nivo}")
                .Match("(p)-[:Pohadja]->(k)")
                .Return(p => new { Polaznik = p.As<Polaznik>() });

            var lista = new List<Polaznik>();
            foreach (var result in query.Results)
                lista.Add(result.Polaznik);

            return lista;
        }

        public void ObrisiKurs(Kurs kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            client.Cypher.Match("(k:Kurs)")
                .WithParams(dictionary)
                .Where("k.Jezik={Jezik} AND k.Nivo = {Nivo}")
                .DetachDelete("k")
                .ExecuteWithoutResults();
        }

        public void IspisiPolaznikaSaKursa(Polaznik polaznik, Kurs kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Ime", polaznik.Ime);
            dictionary.Add("Prezime", polaznik.Prezime);
            dictionary.Add("Email", polaznik.Email);
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            client.Cypher.Match("(p {Ime:{Ime}, Prezime:{Prezime}, Email:{Email}})-[r:Pohadja]->(k {Jezik:{Jezik}, Nivo:{Nivo}})")
                .WithParams(dictionary)
                .Delete("r")
                .ExecuteWithoutResults();
        }

        public Kurs UpdateKurs(Kurs kurs, Kurs stari_kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("StariJezik", stari_kurs.Jezik);
            dictionary.Add("StariNivo", stari_kurs.Nivo);
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);
            dictionary.Add("BrojPolaznika", kurs.BrojPolaznika);

            var novi_kurs = client.Cypher
                .Match("(k:Kurs)")
                .Where("k.Jezik={StariJezik} AND k.Nivo={StariNivo}")
                .WithParams(dictionary)
                .Set("k = {Jezik:{Jezik},Nivo:{Nivo},BrojPolaznika:{BrojPolaznika}}")
                .Return(k => new { Kurs = k.As<Kurs>() });

            return novi_kurs.Results.FirstOrDefault().Kurs;
        }

        public void UpdateKursProfesorom(Kurs kurs, Profesor profesor)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Ime", profesor.Ime);
            dictionary.Add("Prezime", profesor.Prezime);
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            client.Cypher.Match("(k:Kurs {Jezik:{Jezik},Nivo:{Nivo}}),(p:Profesor {Ime:{Ime},Prezime:{Prezime}}), (pp:Profesor)")
               .WithParams(dictionary)
               .Merge("(pp)-[r:Drzi]->(k)")
               .Delete("r")
               .Merge("(p)-[:Drzi]->(k)")
               .ExecuteWithoutResults();
        }

        public void ObrisiVezuProfesor(Kurs kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            client.Cypher.Match("(k:Kurs {Jezik:{Jezik}, Nivo:{Nivo}}),(p:Profesor)")
                .WithParams(dictionary)
                .Merge("(p)-[r:Drzi]->(k)")
                .Delete("r")
                .ExecuteWithoutResults();
        }

        public List<Polaznik> VratiPolaznikeZaDodavanje(Kurs kurs)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            var query = client.Cypher.Match("(k:Kurs {Jezik:{Jezik}, Nivo:{Nivo}}),(p:Polaznik)")
                .WithParams(dictionary)
                .Match("(p) WHERE  NOT (p)-[:Pohadja]->(k)")
                .Return(p => new { Polaznik = p.As<Polaznik>() });

            var lista = new List<Polaznik>();

            foreach (var result in query.Results)
                lista.Add(result.Polaznik);

            return lista;
        }

        public void DodajPolaznikaNaKurs(Kurs kurs, Polaznik polaznik)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Email", polaznik.Email);
            dictionary.Add("Jezik", kurs.Jezik);
            dictionary.Add("Nivo", kurs.Nivo);

            client.Cypher
                .Match("(k:Kurs {Jezik:{Jezik},Nivo:{Nivo}}), (p:Polaznik {Email:{Email}})")
                .WithParams(dictionary)
                .Create("(p)-[:Pohadja]->(k)")
                .ExecuteWithoutResults();
        }

        public List<Kurs> VratiSlobodneKurseveZaProfesore()
        {
            var kursevi = client.Cypher
                .Match("(k:Kurs)")
                .Match("(k) WHERE  NOT ()-[:Drzi]->(k)")
                .Return(k => new { Kurs = k.As<Kurs>() });

            var lista = new List<Kurs>();

            foreach (var kurs in kursevi.Results)
                lista.Add(kurs.Kurs);

            return lista;
        }

        public Profesor DodajProfesora(Profesor profesor)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Ime", profesor.Ime);
            dictionary.Add("Prezime", profesor.Prezime);
            dictionary.Add("Email", profesor.Email);
            dictionary.Add("Telefon", profesor.Telefon);
            dictionary.Add("Adresa", profesor.Adresa);
            dictionary.Add("DatumRodjenja", profesor.DatumRodjenja);

            var query = client.Cypher.Create("(p:Profesor{Ime : {Ime}, Prezime: {Prezime}, Email: {Email}, Telefon: {Telefon}, Adresa: {Adresa}, DatumRodjenja: {DatumRodjenja}})")
                              .WithParams(dictionary)
                              .Return(p => new { Profesor = p.As<Profesor>() });

            return query.Results.FirstOrDefault().Profesor;
        }

        public bool ProveriPostojecegProfesora(Profesor profesor)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Email", profesor.Email);

            var query = client.Cypher
                .Match("(p:Profesor{Email:{Email}})")
                .WithParams(dictionary)
                .Return(p => new { Profesor = p.As<Profesor>() });

            if (query.Results.Any())
                return false;

            return true;

        }

        public void ObrisiProfesora(Profesor profesor)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Email", profesor.Email);

            client.Cypher.Match("(p:Profesor)")
                .WithParams(dictionary)
                .Where("p.Email={Email}")
                .DetachDelete("p")
                .ExecuteWithoutResults();
        }

        public List<Kurs> VratiSlobodneKurseveZaPolaznike()
        {
            var kursevi = client.Cypher
                .Match("()-[r:Pohadja]->(k:Kurs)")
                .With("COUNT(r) as brojVeza, k")
                .Where("brojVeza < k.BrojPolaznika")
                .Return(k => new { Kurs = k.As<Kurs>() })
                .Union()
                .Match("(k:Kurs) WHERE NOT ()-[:Pohadja]->(k)")
                .Return(k => new { Kurs = k.As<Kurs>() });


            var lista = new List<Kurs>();

            foreach (var kurs in kursevi.Results)
            {
                lista.Add(kurs.Kurs);
            }

            return lista;
        }

        public bool ProveriPostojecegPolaznika(Polaznik polaznik)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Email", polaznik.Email);

            var query = client.Cypher
                .Match("(p:Polaznik{Email:{Email}})")
                .WithParams(dictionary)
                .Return(p => new { Polaznik = p.As<Polaznik>() });

            if (query.Results.Any())
                return false;

            return true;
        }

        public Polaznik DodajPolaznika(Polaznik polaznik)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Ime", polaznik.Ime);
            dictionary.Add("Prezime", polaznik.Prezime);
            dictionary.Add("Email", polaznik.Email);
            dictionary.Add("Telefon", polaznik.Telefon);
            dictionary.Add("Adresa", polaznik.Adresa);
            dictionary.Add("DatumRodjenja", polaznik.DatumRodjenja);

            var query = client.Cypher.Create("(p:Polaznik{Ime : {Ime}, Prezime: {Prezime}, Email: {Email}, Telefon: {Telefon}, Adresa: {Adresa}, DatumRodjenja: {DatumRodjenja}})")
                              .WithParams(dictionary)
                              .Return(p => new { Polaznik = p.As<Polaznik>() });

            return query.Results.FirstOrDefault().Polaznik;
        }

        public void ObrisiPolaznika(Polaznik polaznik)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Email", polaznik.Email);

            client.Cypher.Match("(p:Polaznik)")
                .WithParams(dictionary)
                .Where("p.Email={Email}")
                .DetachDelete("p")
                .ExecuteWithoutResults();
        }

        public void AzurirajProfesora(Profesor profesor, string email)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("StariEmail", email);
            dictionary.Add("Ime", profesor.Ime);
            dictionary.Add("Prezime", profesor.Prezime);
            dictionary.Add("Email", profesor.Email);
            dictionary.Add("Telefon", profesor.Telefon);
            dictionary.Add("Adresa", profesor.Adresa);
            dictionary.Add("DatumRodjenja", profesor.DatumRodjenja);



            client.Cypher.Match("(p:Profesor {Email: {StariEmail}})")
                         .WithParams(dictionary)
                         .Set("p = {Ime:{Ime},Prezime:{Prezime},Email:{Email}, Telefon: {Telefon}, Adresa: {Adresa}, DatumRodjenja: {DatumRodjenja}}")
                         .ExecuteWithoutResults();
        }

        public void AzurirajPolaznika(Polaznik polaznik, string email)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("StariEmail", email);
            dictionary.Add("Ime", polaznik.Ime);
            dictionary.Add("Prezime", polaznik.Prezime);
            dictionary.Add("Email", polaznik.Email);
            dictionary.Add("Telefon", polaznik.Telefon);
            dictionary.Add("Adresa", polaznik.Adresa);
            dictionary.Add("DatumRodjenja", polaznik.DatumRodjenja);



            client.Cypher.Match("(p:Polaznik {Email: {StariEmail}})")
                         .WithParams(dictionary)
                         .Set("p = {Ime:{Ime},Prezime:{Prezime},Email:{Email}, Telefon: {Telefon}, Adresa: {Adresa}, DatumRodjenja: {DatumRodjenja}}")
                         .ExecuteWithoutResults();
        }

        public Polaznik VratiPolaznika(Polaznik polaznik)
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add("Email", polaznik.Email);

            var query = client.Cypher.Match("(p:Polaznik {Email: {Email}})")
                .WithParams(dictionary)
                .Return(p => new { Polaznik = p.As<Polaznik>() });

            if (query.Results.Any())
                return query.Results.FirstOrDefault().Polaznik;

            return null;
        }
    }
}
