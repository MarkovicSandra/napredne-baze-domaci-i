﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NPB_Neo4J.DataLayer.Models
{
    public class Polaznik
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }
        public string Telefon { get; set; }
        public string Adresa { get; set; }
        public Nullable<DateTime> DatumRodjenja { get; set; }


        public Polaznik()
        { }

        public Polaznik(string ime, string prezime, string email, string telefon, string adresa, DateTime datumRodjenja)
        {
            Ime = ime;
            Prezime = prezime;
            Email = email;
            Adresa = adresa;
            Telefon = telefon;
            DatumRodjenja = datumRodjenja;
        }

        public override string ToString()
        {
            return Ime + " " + Prezime + " " + Email;
        }

        public static Polaznik SelektovaniPolaznik(string selektovani_polaznik)
        {
            char[] separator = { ' ' };
            var parametri = selektovani_polaznik.Split(separator, 3);

            return new Polaznik { Ime = parametri[0], Prezime = parametri[1], Email = parametri[2] };
        }

        public static bool ProveriObaveznaPolja(string ime, string prezime, string email)
        {
            if (string.IsNullOrWhiteSpace(ime))
            {
                MessageBox.Show("Morate uneti ime polaznika!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(prezime))
            {
                MessageBox.Show("Morate uneti prezime polaznika!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(email))
            {
                MessageBox.Show("Morate uneti email polaznika!");
                return false;
            }

            return true;
        }
    }
}
