﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NPB_Neo4J.DataLayer.Models
{
    public class Kurs
    {
        public string Jezik { get; set; }
        public string Nivo { get; set; }
        public int BrojPolaznika { get; set; }

        public override string ToString()
        {
            return Jezik + " " + Nivo;
        }

        public Kurs()
        { }

        public Kurs(string jezik, string nivo)
        {
            Jezik = jezik;
            Nivo = nivo;
        }

        public Kurs(string jezik, string nivo, int brojpolaznika)
        {
            Jezik = jezik;
            Nivo = nivo;
            BrojPolaznika = brojpolaznika;
        }

        public static Kurs SelektovaniKurs(string selektovani_kurs)
        {
            char[] separator = {' '};
            var parametri = selektovani_kurs.Split(separator, 2);

            return new Kurs { Jezik = parametri[0], Nivo = parametri[1] };
        }

        public static bool ProveriObaveznaPolja(object jezik, object nivo)
        {
            if (jezik == null)
            {
                MessageBox.Show("Unesite zeljeni jezik");
                return false;
            }

            if (nivo == null)
            {
                MessageBox.Show("Unesite zeljeni nivo");
                return false;
            }

            return true;
        }
    }
}
