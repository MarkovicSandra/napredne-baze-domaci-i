﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NPB_Neo4J.DataLayer.Models
{
    public class Profesor
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }
        public string Telefon { get; set; }
        public string Adresa { get; set; }
        public Nullable<DateTime> DatumRodjenja { get; set; }

        public Profesor(string ime, string prezime, string email, string telefon, string adresa, DateTime datumrodjenja)
        {
            Ime = ime;
            Prezime = prezime;
            Email = email;
            Telefon = telefon;
            Adresa = adresa;
            DatumRodjenja = datumrodjenja;
        }

        public Profesor()
        { }

        public override string ToString()
        {
            return Ime + " " + Prezime + " " + Email;
        }

        public static Profesor SelektovaniProfesor(string selektovani_profesor)
        {
            char[] separator = { ' ' };
            var parametri = selektovani_profesor.Split(separator, 3);

            return new Profesor { Ime = parametri[0], Prezime = parametri[1], Email = parametri[2] };
        }

        public static bool ProveriObaveznaPolja(string ime, string prezime, string email)
        {
            if (string.IsNullOrWhiteSpace(ime))
            {
                MessageBox.Show("Morate uneti ime profesora.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(prezime))
            {
                MessageBox.Show("Morate uneti prezime profesora.");
                return false;
            }

            if (string.IsNullOrWhiteSpace(email))
            {
                MessageBox.Show("Morate uneti email profesora.");
                return false;
            }

            return true;
        }
    }
}
