﻿namespace NPB_Neo4J
{
    partial class UpdateKurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxJezik = new System.Windows.Forms.ComboBox();
            this.comboBoxNivo = new System.Windows.Forms.ComboBox();
            this.numericUpDownBrojPolaznika = new System.Windows.Forms.NumericUpDown();
            this.comboBoxProfesor = new System.Windows.Forms.ComboBox();
            this.listViewPolaznici = new System.Windows.Forms.ListView();
            this.buttonAzuriraj = new System.Windows.Forms.Button();
            this.buttonOtkazi = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.IspisiSaKursaButton = new System.Windows.Forms.Button();
            this.ButtonRefresh = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonDodajPolaznika = new System.Windows.Forms.Button();
            this.listViewDodajPolaznika = new System.Windows.Forms.ListView();
            this.buttonRefreshPolaznikeZaDodavanje = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojPolaznika)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jezik";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nivo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Broj polaznika";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Profesor";
            // 
            // comboBoxJezik
            // 
            this.comboBoxJezik.FormattingEnabled = true;
            this.comboBoxJezik.Items.AddRange(new object[] {
            "Engleski",
            "Nemacki",
            "Italijanski",
            "Spanski",
            "Ruski",
            "Francuski",
            "Norveski"});
            this.comboBoxJezik.Location = new System.Drawing.Point(54, 28);
            this.comboBoxJezik.Name = "comboBoxJezik";
            this.comboBoxJezik.Size = new System.Drawing.Size(121, 21);
            this.comboBoxJezik.TabIndex = 4;
            // 
            // comboBoxNivo
            // 
            this.comboBoxNivo.FormattingEnabled = true;
            this.comboBoxNivo.Items.AddRange(new object[] {
            "A1",
            "A2",
            "B1",
            "B2",
            "C1",
            "C2"});
            this.comboBoxNivo.Location = new System.Drawing.Point(54, 60);
            this.comboBoxNivo.Name = "comboBoxNivo";
            this.comboBoxNivo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxNivo.TabIndex = 5;
            // 
            // numericUpDownBrojPolaznika
            // 
            this.numericUpDownBrojPolaznika.Location = new System.Drawing.Point(82, 90);
            this.numericUpDownBrojPolaznika.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDownBrojPolaznika.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownBrojPolaznika.Name = "numericUpDownBrojPolaznika";
            this.numericUpDownBrojPolaznika.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownBrojPolaznika.TabIndex = 6;
            this.numericUpDownBrojPolaznika.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // comboBoxProfesor
            // 
            this.comboBoxProfesor.FormattingEnabled = true;
            this.comboBoxProfesor.Location = new System.Drawing.Point(70, 123);
            this.comboBoxProfesor.Name = "comboBoxProfesor";
            this.comboBoxProfesor.Size = new System.Drawing.Size(132, 21);
            this.comboBoxProfesor.TabIndex = 7;
            // 
            // listViewPolaznici
            // 
            this.listViewPolaznici.HideSelection = false;
            this.listViewPolaznici.Location = new System.Drawing.Point(6, 186);
            this.listViewPolaznici.MultiSelect = false;
            this.listViewPolaznici.Name = "listViewPolaznici";
            this.listViewPolaznici.Size = new System.Drawing.Size(295, 133);
            this.listViewPolaznici.TabIndex = 8;
            this.listViewPolaznici.UseCompatibleStateImageBehavior = false;
            this.listViewPolaznici.View = System.Windows.Forms.View.List;
            // 
            // buttonAzuriraj
            // 
            this.buttonAzuriraj.Location = new System.Drawing.Point(169, 345);
            this.buttonAzuriraj.Name = "buttonAzuriraj";
            this.buttonAzuriraj.Size = new System.Drawing.Size(84, 22);
            this.buttonAzuriraj.TabIndex = 9;
            this.buttonAzuriraj.Text = "Azuriraj";
            this.buttonAzuriraj.UseVisualStyleBackColor = true;
            this.buttonAzuriraj.Click += new System.EventHandler(this.buttonAzuriraj_Click);
            // 
            // buttonOtkazi
            // 
            this.buttonOtkazi.Location = new System.Drawing.Point(54, 345);
            this.buttonOtkazi.Name = "buttonOtkazi";
            this.buttonOtkazi.Size = new System.Drawing.Size(93, 22);
            this.buttonOtkazi.TabIndex = 10;
            this.buttonOtkazi.Text = "Otkazi";
            this.buttonOtkazi.UseVisualStyleBackColor = true;
            this.buttonOtkazi.Click += new System.EventHandler(this.buttonOtkazi_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Polaznici:";
            // 
            // IspisiSaKursaButton
            // 
            this.IspisiSaKursaButton.Location = new System.Drawing.Point(322, 210);
            this.IspisiSaKursaButton.Name = "IspisiSaKursaButton";
            this.IspisiSaKursaButton.Size = new System.Drawing.Size(95, 36);
            this.IspisiSaKursaButton.TabIndex = 12;
            this.IspisiSaKursaButton.Text = "Ispisi sa kursa";
            this.IspisiSaKursaButton.UseVisualStyleBackColor = true;
            this.IspisiSaKursaButton.Click += new System.EventHandler(this.IspisiSaKursaButton_Click);
            // 
            // ButtonRefresh
            // 
            this.ButtonRefresh.Location = new System.Drawing.Point(322, 266);
            this.ButtonRefresh.Name = "ButtonRefresh";
            this.ButtonRefresh.Size = new System.Drawing.Size(95, 34);
            this.ButtonRefresh.TabIndex = 13;
            this.ButtonRefresh.Text = "Refresh";
            this.ButtonRefresh.UseVisualStyleBackColor = true;
            this.ButtonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(435, 422);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.ButtonRefresh);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.IspisiSaKursaButton);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.buttonOtkazi);
            this.tabPage1.Controls.Add(this.comboBoxJezik);
            this.tabPage1.Controls.Add(this.buttonAzuriraj);
            this.tabPage1.Controls.Add(this.comboBoxNivo);
            this.tabPage1.Controls.Add(this.listViewPolaznici);
            this.tabPage1.Controls.Add(this.numericUpDownBrojPolaznika);
            this.tabPage1.Controls.Add(this.comboBoxProfesor);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(427, 396);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Podaci o kursu";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonRefreshPolaznikeZaDodavanje);
            this.tabPage2.Controls.Add(this.buttonDodajPolaznika);
            this.tabPage2.Controls.Add(this.listViewDodajPolaznika);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(427, 396);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Dodaj polaznika";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonDodajPolaznika
            // 
            this.buttonDodajPolaznika.Location = new System.Drawing.Point(89, 223);
            this.buttonDodajPolaznika.Name = "buttonDodajPolaznika";
            this.buttonDodajPolaznika.Size = new System.Drawing.Size(86, 28);
            this.buttonDodajPolaznika.TabIndex = 1;
            this.buttonDodajPolaznika.Text = "Dodaj";
            this.buttonDodajPolaznika.UseVisualStyleBackColor = true;
            this.buttonDodajPolaznika.Click += new System.EventHandler(this.buttonDodajPolaznika_Click);
            // 
            // listViewDodajPolaznika
            // 
            this.listViewDodajPolaznika.HideSelection = false;
            this.listViewDodajPolaznika.Location = new System.Drawing.Point(18, 25);
            this.listViewDodajPolaznika.MultiSelect = false;
            this.listViewDodajPolaznika.Name = "listViewDodajPolaznika";
            this.listViewDodajPolaznika.Size = new System.Drawing.Size(388, 176);
            this.listViewDodajPolaznika.TabIndex = 0;
            this.listViewDodajPolaznika.UseCompatibleStateImageBehavior = false;
            this.listViewDodajPolaznika.View = System.Windows.Forms.View.List;
            // 
            // buttonRefreshPolaznikeZaDodavanje
            // 
            this.buttonRefreshPolaznikeZaDodavanje.Location = new System.Drawing.Point(230, 223);
            this.buttonRefreshPolaznikeZaDodavanje.Name = "buttonRefreshPolaznikeZaDodavanje";
            this.buttonRefreshPolaznikeZaDodavanje.Size = new System.Drawing.Size(81, 28);
            this.buttonRefreshPolaznikeZaDodavanje.TabIndex = 2;
            this.buttonRefreshPolaznikeZaDodavanje.Text = "Refresh";
            this.buttonRefreshPolaznikeZaDodavanje.UseVisualStyleBackColor = true;
            this.buttonRefreshPolaznikeZaDodavanje.Click += new System.EventHandler(this.buttonRefreshPolaznikeZaDodavanje_Click);
            // 
            // UpdateKurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 441);
            this.Controls.Add(this.tabControl1);
            this.Name = "UpdateKurs";
            this.Text = "Kurs";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojPolaznika)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxJezik;
        private System.Windows.Forms.ComboBox comboBoxNivo;
        private System.Windows.Forms.NumericUpDown numericUpDownBrojPolaznika;
        private System.Windows.Forms.ComboBox comboBoxProfesor;
        private System.Windows.Forms.ListView listViewPolaznici;
        private System.Windows.Forms.Button buttonAzuriraj;
        private System.Windows.Forms.Button buttonOtkazi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button IspisiSaKursaButton;
        private System.Windows.Forms.Button ButtonRefresh;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonDodajPolaznika;
        private System.Windows.Forms.ListView listViewDodajPolaznika;
        private System.Windows.Forms.Button buttonRefreshPolaznikeZaDodavanje;
    }
}