﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Neo4J.DataLayer;
using NPB_Neo4J.DataLayer.Models;

namespace NPB_Neo4J
{
    public partial class UpdatePolaznika : Form
    {
        public DataProvider data_provider = new DataProvider();
        public Polaznik Polaznik;

        public UpdatePolaznika(Polaznik polaznik)
        {
            InitializeComponent();
            this.Text = polaznik.Ime + " " + polaznik.Prezime;
            textBoxIme.Text = polaznik.Ime;
            textBoxPrezime.Text = polaznik.Prezime;
            textBoxEmail.Text = polaznik.Email;
            textBoxTelefon.Text = polaznik.Telefon;
            textBoxAdresa.Text = polaznik.Adresa;
            dateTimePicker1.Value = (DateTime)polaznik.DatumRodjenja;

            Polaznik = polaznik;
        }


        private void buttonAzuriraj_Click(object sender, EventArgs e)
        {
            var ime = textBoxIme.Text;
            var prezime = textBoxPrezime.Text;
            var email = textBoxEmail.Text;

            if (!Polaznik.ProveriObaveznaPolja(ime, prezime, email))
                return;

            var telefon = textBoxTelefon.Text;
            var adresa = textBoxAdresa.Text;
            var datumRodjenja = dateTimePicker1.Value;


            var polaznik = new Polaznik(ime, prezime, email, telefon, adresa, datumRodjenja);

            if (!Polaznik.Email.Equals(polaznik.Email))
            {
                if (!data_provider.ProveriPostojecegPolaznika(polaznik))
                {
                    MessageBox.Show("Polaznik sa navedenim emailom vec postoji!");
                    return;
                }
            }

            data_provider.AzurirajPolaznika(polaznik, Polaznik.Email);

            this.Close();
        }

    }
}
