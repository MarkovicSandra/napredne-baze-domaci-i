﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Neo4J.DataLayer.Models;
using NPB_Neo4J.DataLayer;

namespace NPB_Neo4J
{
    public partial class Form1 : Form
    {
        DataProvider data_provider = new DataProvider();

        public Form1()
        {
            InitializeComponent();
            PopuniKontrole();
        }

        
        public void PopuniKontrole()
        {
            this.PopuniListuKurseva(data_provider.VratiSveKurseve());
            this.PopuniListuProfesora(data_provider.VratiSveProfesore());
            this.PopuniListuPolaznika(data_provider.VratiSvePolaznike());
            this.PopuniPretraguKurseva(data_provider.VratiSveKurseve());
        }

        public void PopuniListuKurseva(List<Kurs> kursevi)
        {
            foreach (var kurs in kursevi)
                listViewKursevi.Items.Add(new ListViewItem(kurs.ToString()));
        }

        public void PopuniPretraguKurseva(List<Kurs> kursevi)
        {
            var razliciti_jezici = kursevi.Select(x => x.Jezik).Distinct();
            var razliciti_nivoi = kursevi.Select(x => x.Nivo).Distinct();

            foreach (var jezik in razliciti_jezici)
                comboBoxJezik.Items.Add(jezik);

            foreach (var nivo in razliciti_nivoi)
                comboBoxNivo.Items.Add(nivo);
        }

        public void PopuniListuProfesora(List<Profesor> profesori)
        {
            foreach (var profesor in profesori)
                listViewProfesori.Items.Add(new ListViewItem(profesor.ToString()));
        }

        public void PopuniListuPolaznika(List<Polaznik> polaznici)
        {
            foreach (var polaznik in polaznici)
                listViewPolaznici.Items.Add(new ListViewItem(polaznik.ToString()));
        }

        private void ButtonDodajKurs_Click(object sender, EventArgs e)
        {
            DodajKurs forma = new DodajKurs();
            forma.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DodajPolaznika forma = new DodajPolaznika();
            forma.Show();
        }

        private void DodajProfesora_Click(object sender, EventArgs e)
        {
            DodajProfesora forma = new DodajProfesora();
            forma.Show();
        }


        private void buttonPonistiFiltereKurs_Click(object sender, EventArgs e)
        {
            listViewKursevi.Clear();
            this.PopuniListuKurseva(data_provider.VratiSveKurseve());
        }

        private void PretraziKurs_Click(object sender, EventArgs e)
        {
            string jezik = null;
            string nivo = null;

            if (comboBoxJezik.SelectedItem != null)
                jezik = comboBoxJezik.SelectedItem.ToString();

            if (comboBoxNivo.SelectedItem != null)
                nivo = comboBoxNivo.SelectedItem.ToString();

            Kurs k =  new Kurs(jezik, nivo);

            var kursevi = data_provider.PretraziKurs(k);

            listViewKursevi.Clear();

            this.PopuniListuKurseva(kursevi);
        }

        private void KursRefreshButton_Click(object sender, EventArgs e)
        {
            listViewKursevi.Clear();
            var prethodno_izabrani_jezik= comboBoxJezik.SelectedItem;
            var prethodno_izabrani_nivo = comboBoxNivo.SelectedItem;
            comboBoxJezik.Items.Clear();
            comboBoxNivo.Items.Clear();
            this.PopuniListuKurseva(data_provider.VratiSveKurseve());
            this.PopuniPretraguKurseva(data_provider.VratiSveKurseve());
            comboBoxJezik.SelectedItem = prethodno_izabrani_jezik;
            comboBoxNivo.SelectedItem = prethodno_izabrani_nivo;
        }

        private void listViewKursevi_ItemActivate(object sender, EventArgs e)
        {
            var obj = (ListView)sender;
            string selektovani_kurs= obj.FocusedItem.Text;

            Kurs kurs = Kurs.SelektovaniKurs(selektovani_kurs);

            Kurs novi_kurs = data_provider.PretraziKurs(kurs).ElementAt(0);

            UpdateKurs forma = new UpdateKurs(novi_kurs);

            forma.Show();
        }

        private void ObrisiKursButton_Click(object sender, EventArgs e)
        {
            if (listViewKursevi.FocusedItem != null)
            {
                string selektovani_kurs = listViewKursevi.FocusedItem.Text;
                Kurs kurs = Kurs.SelektovaniKurs(selektovani_kurs);

                data_provider.ObrisiKurs(kurs);

                listViewKursevi.Items.Remove(listViewKursevi.FocusedItem);
            }
        }

        private void buttonRefreshProfesori_Click(object sender, EventArgs e)
        {
            listViewProfesori.Items.Clear();

            this.PopuniListuProfesora(data_provider.VratiSveProfesore());
        }

        private void buttonObrisiProfesora_Click(object sender, EventArgs e)
        {
            if (listViewProfesori.FocusedItem != null)
            {
                string selektovani_profesor = listViewProfesori.FocusedItem.Text;

                Profesor profesor = Profesor.SelektovaniProfesor(selektovani_profesor);

                data_provider.ObrisiProfesora(profesor);

                listViewProfesori.Items.Remove(listViewProfesori.FocusedItem);
            }
        }

        private void buttonRefreshPolaznike_Click(object sender, EventArgs e)
        {
            listViewPolaznici.Items.Clear();
            this.PopuniListuPolaznika(data_provider.VratiSvePolaznike());
        }

        private void buttonObrisiPolaznika_Click(object sender, EventArgs e)
        {
            if (listViewPolaznici.FocusedItem != null)
            {
                string selektovani_polaznik = listViewPolaznici.FocusedItem.Text;

                Polaznik polaznik = Polaznik.SelektovaniPolaznik(selektovani_polaznik);

                data_provider.ObrisiPolaznika(polaznik);

                listViewPolaznici.Items.Remove(listViewPolaznici.FocusedItem);
            }
        }

        private void listViewProfesori_ItemActivate(object sender, EventArgs e)
        {
            var obj = (ListView)sender;
            string selektovani_profesor = obj.FocusedItem.Text;

            var profesor = Profesor.SelektovaniProfesor(selektovani_profesor);
            profesor = data_provider.VratiProfesora(profesor);

            UpdateProfesora forma = new UpdateProfesora(profesor);

            forma.Show();
        }

        private void listViewPolaznici_ItemActivate(object sender, EventArgs e)
        {
            var obj = (ListView)sender;
            string selektovani_polaznik = obj.FocusedItem.Text;

            var polaznik = Polaznik.SelektovaniPolaznik(selektovani_polaznik);
            polaznik = data_provider.VratiPolaznika(polaznik);

            UpdatePolaznika forma = new UpdatePolaznika(polaznik);

            forma.Show();
        }
    }
}
