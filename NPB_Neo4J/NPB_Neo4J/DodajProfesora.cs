﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Neo4J.DataLayer;
using NPB_Neo4J.DataLayer.Models;

namespace NPB_Neo4J
{
    public partial class DodajProfesora : Form
    {
        public DataProvider data_provider = new DataProvider();

        public DodajProfesora()
        {
            InitializeComponent();

            var dostupni_kursevi = data_provider.VratiSlobodneKurseveZaProfesore();

            foreach (var kurs in dostupni_kursevi)
                comboBoxKursevi.Items.Add(kurs.ToString());
        }

        private void ButtonDodaj_Click(object sender, EventArgs e)
        {
            var ime = textBoxIme.Text;
            var prezime = textBoxPrezime.Text;
            var email = textBoxEmail.Text;

            if (!Profesor.ProveriObaveznaPolja(ime, prezime, email))
                return;

            var telefon = textBoxTelefon.Text;
            var adresa = textBoxAdresa.Text;
            var datumRodjenja = dateTimePicker1.Value;

          
            var profesor = new Profesor(ime, prezime, email, telefon, adresa, datumRodjenja);

            if (!data_provider.ProveriPostojecegProfesora(profesor))
            {
                MessageBox.Show("Profesor sa navedenim emailom vec postoji!");
                return;
            }

            profesor = data_provider.DodajProfesora(profesor);

            Kurs selektovaniKurs;
            if (comboBoxKursevi.SelectedItem != null)
            {
                selektovaniKurs = Kurs.SelektovaniKurs(comboBoxKursevi.SelectedItem.ToString());
                data_provider.UpdateKursProfesorom(selektovaniKurs, profesor);
            }

            this.Close();
        }

    }
}
