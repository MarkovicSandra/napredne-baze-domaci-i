﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Neo4J.DataLayer.Models;
using NPB_Neo4J.DataLayer;

namespace NPB_Neo4J
{
    public partial class UpdateKurs : Form
    {
        private DataProvider data_provider;
        private Kurs Kurs = new Kurs();
        private Profesor Profesor;

        public UpdateKurs()
        {
            InitializeComponent();
        }

        public UpdateKurs(Kurs kurs)
        {
            InitializeComponent();
            data_provider = new DataProvider();
            PopuniKontrole(kurs);
        }

        public void PopuniKontrole(Kurs kurs)
        {
            numericUpDownBrojPolaznika.Minimum = data_provider.VratiPolaznike(kurs).Count;
            numericUpDownBrojPolaznika.Value = kurs.BrojPolaznika;
            this.Text += " " + kurs.Jezik + " " + kurs.Nivo;
            PopuniCombobox(kurs);
        }

        public void PopuniCombobox(Kurs kurs)
        {
     
            comboBoxJezik.SelectedItem = kurs.Jezik;
            comboBoxNivo.SelectedItem = kurs.Nivo;

            var profesori = data_provider.VratiSveProfesore();
            var postojeci_profesor = data_provider.VratiProfesora(kurs);

            foreach (var profesor in profesori)
                comboBoxProfesor.Items.Add(profesor.ToString());


            if (postojeci_profesor != null)
            {
                comboBoxProfesor.SelectedItem = postojeci_profesor.ToString();
                Profesor = postojeci_profesor;
            }

            var polaznici = data_provider.VratiPolaznike(kurs);

            if (polaznici != null)
            {
                foreach(var polaznik in polaznici)
                    listViewPolaznici.Items.Add(new ListViewItem(polaznik.ToString()));
            }

            var polazniciZaDodavanje = data_provider.VratiPolaznikeZaDodavanje(kurs);

            if (polazniciZaDodavanje != null)
            {
                foreach (var polaznik in polazniciZaDodavanje)
                    listViewDodajPolaznika.Items.Add(new ListViewItem(polaznik.ToString()));
            }

            Kurs = kurs;
        }

        private void buttonOtkazi_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAzuriraj_Click(object sender, EventArgs e)
        {
            var jezik = comboBoxJezik.SelectedItem;
            var nivo = comboBoxNivo.SelectedItem;

            if (!Kurs.ProveriObaveznaPolja(jezik, nivo))
            {
                return;
            }

            int brojPolaznika = (int)numericUpDownBrojPolaznika.Value;

            var kurs = new Kurs(jezik.ToString(), nivo.ToString(), brojPolaznika);

            if (!Kurs.Jezik.Equals(kurs.Jezik) || !Kurs.Nivo.Equals(kurs.Nivo))
            {
                var kursevi = data_provider.PretraziKurs(kurs);

                if (kursevi.Any())
                {
                    MessageBox.Show("Postoji kurs sa izabranim jezikom i nivoom!");
                    return;
                }
            }

            var azurirani_kurs = data_provider.UpdateKurs(kurs, Kurs);

            Kurs = azurirani_kurs;

            var profesorCombobox = comboBoxProfesor.SelectedItem;

            if (profesorCombobox != null)
            {
                var profesor = Profesor.SelektovaniProfesor(profesorCombobox.ToString());
                data_provider.UpdateKursProfesorom(Kurs, profesor);
            }
            else
            {
                data_provider.ObrisiVezuProfesor(Kurs);
            }

            this.Close();
        }

        private void IspisiSaKursaButton_Click(object sender, EventArgs e)
        {
            if (listViewPolaznici.FocusedItem != null)
            {
                string selektovani_polaznik = listViewPolaznici.FocusedItem.Text;
                Polaznik polaznik = Polaznik.SelektovaniPolaznik(selektovani_polaznik);

                var jezik = comboBoxJezik.SelectedItem.ToString();
                var nivo = comboBoxNivo.SelectedItem.ToString();

                Kurs kurs = new Kurs(jezik, nivo);

                data_provider.IspisiPolaznikaSaKursa(polaznik, kurs);

                listViewPolaznici.Items.Remove(listViewPolaznici.FocusedItem);
            }
        }

        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            var polaznici = data_provider.VratiPolaznike(Kurs);

            listViewPolaznici.Items.Clear();

            if (polaznici != null)
            {
                foreach (var polaznik in polaznici)
                    listViewPolaznici.Items.Add(new ListViewItem(polaznik.ToString()));
            }
        }

        private void buttonDodajPolaznika_Click(object sender, EventArgs e)
        {
            if (listViewDodajPolaznika.FocusedItem != null)
            {
                var trenutniBrojPolaznika = data_provider.VratiPolaznike(Kurs).Count;

                if (Kurs.BrojPolaznika == trenutniBrojPolaznika)
                {
                    MessageBox.Show("Ne mozete dodati polaznika na kurs.");
                    return;
                }

                string selektovani_polaznik = listViewDodajPolaznika.FocusedItem.Text;
                Polaznik polaznik = Polaznik.SelektovaniPolaznik(selektovani_polaznik);

                data_provider.DodajPolaznikaNaKurs(Kurs, polaznik);

                listViewDodajPolaznika.Items.Remove(listViewDodajPolaznika.FocusedItem);
            }
        }

        private void buttonRefreshPolaznikeZaDodavanje_Click(object sender, EventArgs e)
        {
            listViewDodajPolaznika.Items.Clear();

            var polaznici_za_dodavanje = data_provider.VratiPolaznikeZaDodavanje(Kurs);

            foreach (var polaznik in polaznici_za_dodavanje)
                listViewDodajPolaznika.Items.Add(new ListViewItem(polaznik.ToString()));
        }
    }
}
