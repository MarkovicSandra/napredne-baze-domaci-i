﻿namespace NPB_Neo4J
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ObrisiKursButton = new System.Windows.Forms.Button();
            this.KursRefreshButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonPonistiFiltereKurs = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.PretraziKurs = new System.Windows.Forms.Button();
            this.comboBoxNivo = new System.Windows.Forms.ComboBox();
            this.comboBoxJezik = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ButtonDodajKurs = new System.Windows.Forms.Button();
            this.listViewKursevi = new System.Windows.Forms.ListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonObrisiProfesora = new System.Windows.Forms.Button();
            this.buttonRefreshProfesori = new System.Windows.Forms.Button();
            this.DodajProfesora = new System.Windows.Forms.Button();
            this.listViewProfesori = new System.Windows.Forms.ListView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonObrisiPolaznika = new System.Windows.Forms.Button();
            this.buttonRefreshPolaznike = new System.Windows.Forms.Button();
            this.buttonDodajNovogPolaznika = new System.Windows.Forms.Button();
            this.listViewPolaznici = new System.Windows.Forms.ListView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Tag = "";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ObrisiKursButton);
            this.tabPage1.Controls.Add(this.KursRefreshButton);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.ButtonDodajKurs);
            this.tabPage1.Controls.Add(this.listViewKursevi);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ObrisiKursButton
            // 
            resources.ApplyResources(this.ObrisiKursButton, "ObrisiKursButton");
            this.ObrisiKursButton.Name = "ObrisiKursButton";
            this.ObrisiKursButton.UseVisualStyleBackColor = true;
            this.ObrisiKursButton.Click += new System.EventHandler(this.ObrisiKursButton_Click);
            // 
            // KursRefreshButton
            // 
            resources.ApplyResources(this.KursRefreshButton, "KursRefreshButton");
            this.KursRefreshButton.Name = "KursRefreshButton";
            this.KursRefreshButton.UseVisualStyleBackColor = true;
            this.KursRefreshButton.Click += new System.EventHandler(this.KursRefreshButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonPonistiFiltereKurs);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.PretraziKurs);
            this.groupBox2.Controls.Add(this.comboBoxNivo);
            this.groupBox2.Controls.Add(this.comboBoxJezik);
            this.groupBox2.Controls.Add(this.label3);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // buttonPonistiFiltereKurs
            // 
            resources.ApplyResources(this.buttonPonistiFiltereKurs, "buttonPonistiFiltereKurs");
            this.buttonPonistiFiltereKurs.Name = "buttonPonistiFiltereKurs";
            this.buttonPonistiFiltereKurs.UseVisualStyleBackColor = true;
            this.buttonPonistiFiltereKurs.Click += new System.EventHandler(this.buttonPonistiFiltereKurs_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // PretraziKurs
            // 
            resources.ApplyResources(this.PretraziKurs, "PretraziKurs");
            this.PretraziKurs.Name = "PretraziKurs";
            this.PretraziKurs.UseVisualStyleBackColor = true;
            this.PretraziKurs.Click += new System.EventHandler(this.PretraziKurs_Click);
            // 
            // comboBoxNivo
            // 
            this.comboBoxNivo.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxNivo, "comboBoxNivo");
            this.comboBoxNivo.Name = "comboBoxNivo";
            // 
            // comboBoxJezik
            // 
            this.comboBoxJezik.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxJezik, "comboBoxJezik");
            this.comboBoxJezik.Name = "comboBoxJezik";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // ButtonDodajKurs
            // 
            resources.ApplyResources(this.ButtonDodajKurs, "ButtonDodajKurs");
            this.ButtonDodajKurs.Name = "ButtonDodajKurs";
            this.ButtonDodajKurs.UseVisualStyleBackColor = true;
            this.ButtonDodajKurs.Click += new System.EventHandler(this.ButtonDodajKurs_Click);
            // 
            // listViewKursevi
            // 
            this.listViewKursevi.HideSelection = false;
            resources.ApplyResources(this.listViewKursevi, "listViewKursevi");
            this.listViewKursevi.MultiSelect = false;
            this.listViewKursevi.Name = "listViewKursevi";
            this.listViewKursevi.UseCompatibleStateImageBehavior = false;
            this.listViewKursevi.View = System.Windows.Forms.View.List;
            this.listViewKursevi.ItemActivate += new System.EventHandler(this.listViewKursevi_ItemActivate);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonObrisiProfesora);
            this.tabPage2.Controls.Add(this.buttonRefreshProfesori);
            this.tabPage2.Controls.Add(this.DodajProfesora);
            this.tabPage2.Controls.Add(this.listViewProfesori);
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonObrisiProfesora
            // 
            resources.ApplyResources(this.buttonObrisiProfesora, "buttonObrisiProfesora");
            this.buttonObrisiProfesora.Name = "buttonObrisiProfesora";
            this.buttonObrisiProfesora.UseVisualStyleBackColor = true;
            this.buttonObrisiProfesora.Click += new System.EventHandler(this.buttonObrisiProfesora_Click);
            // 
            // buttonRefreshProfesori
            // 
            resources.ApplyResources(this.buttonRefreshProfesori, "buttonRefreshProfesori");
            this.buttonRefreshProfesori.Name = "buttonRefreshProfesori";
            this.buttonRefreshProfesori.UseVisualStyleBackColor = true;
            this.buttonRefreshProfesori.Click += new System.EventHandler(this.buttonRefreshProfesori_Click);
            // 
            // DodajProfesora
            // 
            resources.ApplyResources(this.DodajProfesora, "DodajProfesora");
            this.DodajProfesora.Name = "DodajProfesora";
            this.DodajProfesora.UseVisualStyleBackColor = true;
            this.DodajProfesora.Click += new System.EventHandler(this.DodajProfesora_Click);
            // 
            // listViewProfesori
            // 
            this.listViewProfesori.HideSelection = false;
            resources.ApplyResources(this.listViewProfesori, "listViewProfesori");
            this.listViewProfesori.MultiSelect = false;
            this.listViewProfesori.Name = "listViewProfesori";
            this.listViewProfesori.UseCompatibleStateImageBehavior = false;
            this.listViewProfesori.View = System.Windows.Forms.View.List;
            this.listViewProfesori.ItemActivate += new System.EventHandler(this.listViewProfesori_ItemActivate);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.buttonObrisiPolaznika);
            this.tabPage3.Controls.Add(this.buttonRefreshPolaznike);
            this.tabPage3.Controls.Add(this.buttonDodajNovogPolaznika);
            this.tabPage3.Controls.Add(this.listViewPolaznici);
            resources.ApplyResources(this.tabPage3, "tabPage3");
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // buttonObrisiPolaznika
            // 
            resources.ApplyResources(this.buttonObrisiPolaznika, "buttonObrisiPolaznika");
            this.buttonObrisiPolaznika.Name = "buttonObrisiPolaznika";
            this.buttonObrisiPolaznika.UseVisualStyleBackColor = true;
            this.buttonObrisiPolaznika.Click += new System.EventHandler(this.buttonObrisiPolaznika_Click);
            // 
            // buttonRefreshPolaznike
            // 
            resources.ApplyResources(this.buttonRefreshPolaznike, "buttonRefreshPolaznike");
            this.buttonRefreshPolaznike.Name = "buttonRefreshPolaznike";
            this.buttonRefreshPolaznike.UseVisualStyleBackColor = true;
            this.buttonRefreshPolaznike.Click += new System.EventHandler(this.buttonRefreshPolaznike_Click);
            // 
            // buttonDodajNovogPolaznika
            // 
            resources.ApplyResources(this.buttonDodajNovogPolaznika, "buttonDodajNovogPolaznika");
            this.buttonDodajNovogPolaznika.Name = "buttonDodajNovogPolaznika";
            this.buttonDodajNovogPolaznika.UseVisualStyleBackColor = true;
            this.buttonDodajNovogPolaznika.Click += new System.EventHandler(this.button2_Click);
            // 
            // listViewPolaznici
            // 
            this.listViewPolaznici.HideSelection = false;
            resources.ApplyResources(this.listViewPolaznici, "listViewPolaznici");
            this.listViewPolaznici.MultiSelect = false;
            this.listViewPolaznici.Name = "listViewPolaznici";
            this.listViewPolaznici.UseCompatibleStateImageBehavior = false;
            this.listViewPolaznici.View = System.Windows.Forms.View.List;
            this.listViewPolaznici.ItemActivate += new System.EventHandler(this.listViewPolaznici_ItemActivate);
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ButtonDodajKurs;
        private System.Windows.Forms.ListView listViewKursevi;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox comboBoxNivo;
        private System.Windows.Forms.ComboBox comboBoxJezik;
        private System.Windows.Forms.Button PretraziKurs;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button DodajProfesora;
        private System.Windows.Forms.ListView listViewProfesori;
        private System.Windows.Forms.Button buttonDodajNovogPolaznika;
        private System.Windows.Forms.ListView listViewPolaznici;
        private System.Windows.Forms.Button buttonPonistiFiltereKurs;
        private System.Windows.Forms.Button KursRefreshButton;
        private System.Windows.Forms.Button ObrisiKursButton;
        private System.Windows.Forms.Button buttonObrisiProfesora;
        private System.Windows.Forms.Button buttonRefreshProfesori;
        private System.Windows.Forms.Button buttonObrisiPolaznika;
        private System.Windows.Forms.Button buttonRefreshPolaznike;
    }
}

