﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Neo4J.DataLayer;
using NPB_Neo4J.DataLayer.Models;

namespace NPB_Neo4J
{
    public partial class UpdateProfesora : Form
    {
        DataProvider data_provider = new DataProvider();
        Profesor Profesor;

        public UpdateProfesora(Profesor profesor)
        {
            InitializeComponent();
            this.Text = profesor.Ime + " " + profesor.Prezime;
            textBoxIme.Text = profesor.Ime;
            textBoxPrezime.Text = profesor.Prezime;
            textBoxEmail.Text = profesor.Email;
            textBoxTelefon.Text = profesor.Telefon;
            textBoxAdresa.Text = profesor.Adresa;
            dateTimePicker1.Value = (DateTime)profesor.DatumRodjenja;

            Profesor = profesor;
        }

        private void buttonAzuriraj_Click(object sender, EventArgs e)
        {
            var ime = textBoxIme.Text;
            var prezime = textBoxPrezime.Text;
            var email = textBoxEmail.Text;

            if (!Profesor.ProveriObaveznaPolja(ime, prezime, email))
                return;

            var telefon = textBoxTelefon.Text;
            var adresa = textBoxAdresa.Text;
            var datumRodjenja = dateTimePicker1.Value;


            var profesor = new Profesor(ime, prezime, email, telefon, adresa, datumRodjenja);

            if (!Profesor.Email.Equals(profesor.Email))
            {
                if (!data_provider.ProveriPostojecegProfesora(profesor))
                {
                    MessageBox.Show("Profesor sa navedenim emailom vec postoji!");
                    return;
                }
            }

            data_provider.AzurirajProfesora(profesor, Profesor.Email);

            this.Close();
        }
    }
}
