﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Neo4J.DataLayer.Models;
using NPB_Neo4J.DataLayer;

namespace NPB_Neo4J
{
    public partial class DodajKurs : Form
    {
        public DataProvider data_provider = new DataProvider();

        public DodajKurs()
        {
            InitializeComponent();
        }

        private void ButtonDodaj_Click(object sender, EventArgs e)
        {
            var jezik = comboBoxJezik.SelectedItem;
            var nivo = comboBoxNivo.SelectedItem;

            if (!Kurs.ProveriObaveznaPolja(jezik, nivo))
                return;

            var brojpolaznika = (int)numericUpDownBrojPolaznika.Value;

            Kurs kurs = new Kurs(jezik.ToString(), nivo.ToString(), brojpolaznika);

            try
            {
                data_provider.DodajKurs(kurs);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private bool ProveriPodatke(object jezik, object nivo)
        {
            if (jezik == null)
            {
                MessageBox.Show("Unesite zeljeni jezik");
                return false;
            }

            if (nivo == null)
            {
                MessageBox.Show("Unesite zeljeni nivo");
                return false;
            }

            return true;
        }
    }
}
