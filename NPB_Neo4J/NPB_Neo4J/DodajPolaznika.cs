﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Neo4J.DataLayer;
using NPB_Neo4J.DataLayer.Models;

namespace NPB_Neo4J
{
    public partial class DodajPolaznika : Form
    {
        public DataProvider data_provider = new DataProvider();

        public DodajPolaznika()
        {
            InitializeComponent();

            var dostupni_kursevi = data_provider.VratiSlobodneKurseveZaPolaznike();

            foreach (var kurs in dostupni_kursevi)
                comboBoxKurseviPolaznik.Items.Add(kurs.ToString());
        }

        private void ButtonDodaj_Click(object sender, EventArgs e)
        {
            var ime = textBoxIme.Text;
            var prezime = textBoxPrezime.Text;
            var email = textBoxEmail.Text;

            if (!Polaznik.ProveriObaveznaPolja(ime, prezime, email))
                return;

            var telefon = textBoxTelefon.Text;
            var adresa = textBoxAdresa.Text;
            var datumRodjenja = dateTimePicker1.Value;

            var polaznik = new Polaznik(ime, prezime, email, telefon, adresa, datumRodjenja);

            if (!data_provider.ProveriPostojecegPolaznika(polaznik))
            {
                MessageBox.Show("Profesor sa navedenim emailom vec postoji!");
                return;
            }

            polaznik = data_provider.DodajPolaznika(polaznik);

            Kurs selektovaniKurs;

            if (comboBoxKurseviPolaznik.SelectedItem != null)
            {
                selektovaniKurs = Kurs.SelektovaniKurs(comboBoxKurseviPolaznik.SelectedItem.ToString());
                data_provider.DodajPolaznikaNaKurs(selektovaniKurs, polaznik);
            }

            this.Close();
        }
    }
}
