﻿namespace NPB_Neo4J
{
    partial class DodajKurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownBrojPolaznika = new System.Windows.Forms.NumericUpDown();
            this.comboBoxNivo = new System.Windows.Forms.ComboBox();
            this.comboBoxJezik = new System.Windows.Forms.ComboBox();
            this.ButtonDodaj = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojPolaznika)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jezik:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nivo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Broj polaznika: ";
            // 
            // numericUpDownBrojPolaznika
            // 
            this.numericUpDownBrojPolaznika.Location = new System.Drawing.Point(111, 103);
            this.numericUpDownBrojPolaznika.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDownBrojPolaznika.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDownBrojPolaznika.Name = "numericUpDownBrojPolaznika";
            this.numericUpDownBrojPolaznika.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownBrojPolaznika.TabIndex = 3;
            this.numericUpDownBrojPolaznika.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // comboBoxNivo
            // 
            this.comboBoxNivo.FormattingEnabled = true;
            this.comboBoxNivo.Items.AddRange(new object[] {
            "A1",
            "A2",
            "B1",
            "B2",
            "C1",
            "C2"});
            this.comboBoxNivo.Location = new System.Drawing.Point(110, 75);
            this.comboBoxNivo.Name = "comboBoxNivo";
            this.comboBoxNivo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxNivo.TabIndex = 4;
            // 
            // comboBoxJezik
            // 
            this.comboBoxJezik.FormattingEnabled = true;
            this.comboBoxJezik.Items.AddRange(new object[] {
            "Engleski",
            "Nemacki",
            "Italijanski",
            "Spanski",
            "Ruski",
            "Francuski",
            "Norveski"});
            this.comboBoxJezik.Location = new System.Drawing.Point(110, 48);
            this.comboBoxJezik.Name = "comboBoxJezik";
            this.comboBoxJezik.Size = new System.Drawing.Size(121, 21);
            this.comboBoxJezik.TabIndex = 5;
            // 
            // ButtonDodaj
            // 
            this.ButtonDodaj.Location = new System.Drawing.Point(110, 174);
            this.ButtonDodaj.Name = "ButtonDodaj";
            this.ButtonDodaj.Size = new System.Drawing.Size(75, 23);
            this.ButtonDodaj.TabIndex = 6;
            this.ButtonDodaj.Text = "Dodaj ";
            this.ButtonDodaj.UseVisualStyleBackColor = true;
            this.ButtonDodaj.Click += new System.EventHandler(this.ButtonDodaj_Click);
            // 
            // DodajKurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 219);
            this.Controls.Add(this.ButtonDodaj);
            this.Controls.Add(this.comboBoxJezik);
            this.Controls.Add(this.comboBoxNivo);
            this.Controls.Add(this.numericUpDownBrojPolaznika);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DodajKurs";
            this.Text = "Dodaj kurs";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojPolaznika)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownBrojPolaznika;
        private System.Windows.Forms.ComboBox comboBoxNivo;
        private System.Windows.Forms.ComboBox comboBoxJezik;
        private System.Windows.Forms.Button ButtonDodaj;
    }
}